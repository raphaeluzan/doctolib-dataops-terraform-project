# Define variables
variable "region" {
  default = "eu-west-3"
  type = string
}

variable "ip_user" {
  default = "eu-west-3"
  type = string
}

variable "public_key" {
  type = string
}

variable "instance_type" {
  type = string
  default = "t2.micro"
}

provider "aws" {
  region = var.region
}

# Get the last official ubuntu-focal-20.04-amd64 image available 
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_security_group" "ssh_access" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = "vpc-aacd04c2"

  ingress {
    description      = "SSH from VPC"
    from_port        = 0
    to_port          = 1100
    protocol         = "tcp"
    cidr_blocks      = [var.ip_user]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key-byterraform"
  public_key = var.public_key
}

resource "aws_iam_policy" "s3access" {
  name        = "s3ec2test"
  description = "allow write access to specific bucket for can_be_written"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Sid": "VisualEditor0",
          "Effect": "Allow",
          "Action": "s3:PutObject",
          "Resource": "arn:aws:s3:::bucket-name-example2/can_be_written"
      }
  ]
}
EOF
}

resource "aws_iam_role" "my_instance" {
  name        = "s3ec2test"
  description = "privileges for the test instance"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "s3access" {
  role       = aws_iam_role.my_instance.name
  policy_arn = aws_iam_policy.s3access.arn
}

resource "aws_iam_instance_profile" "my_instance" {
  name = aws_iam_role.my_instance.name
  role = aws_iam_role.my_instance.id
}

# Create EC2 instance
resource "aws_instance" "my_instance" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name = resource.aws_key_pair.deployer.key_name
  security_groups = [resource.aws_security_group.ssh_access.name]
  iam_instance_profile = aws_iam_instance_profile.my_instance.name
	user_data = <<-EOF
  #!/bin/bash
  sudo apt-get update
  curl -fsSL https://get.docker.com -o get-docker.sh
  sh get-docker.sh
  sudo apt install -y docker-compose
  sudo docker pull mockserver/mockserver
  sudo docker run -d --rm -p 1080:1080 mockserver/mockserver
  EOF
}

# Create S3 Bucket
resource "aws_s3_bucket" "b" {
  bucket = "bucket-name-example2"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}

# Get eip adresse of my EC2 instance (public_ip of the instance is not working)
resource "aws_eip" "lb" {
  instance = aws_instance.my_instance.id
  vpc      = true
}

# Create route 53 health checks
resource "aws_route53_health_check" "health_check_example" {
  ip_address        = resource.aws_eip.lb.public_ip
  port              = 1080
  type              = "HTTP"
  resource_path     = "/mockserver/dashboard"
  failure_threshold = "5"
  request_interval  = "30"

  tags = {
    Name = "tf-test-health-check"
  }
}

