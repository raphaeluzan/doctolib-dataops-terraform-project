# doctolib-dataops-terraform-project

## Steps 
1. Create a simple EC2 instance with terraform  [EC2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) ✅

2. Make sure this vm is accessible only with from my IP, make sure IP is a variable. [ACL](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_acl) ✅

3. create a S3 Bucket. [S3](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) ✅

4. have docker & docker-compose on the VM (not recreate vm each time: 🔴) ✅
5. Use a specific docker image ✅
![ui](./images/ui.png)

6. (Bonus) Ensure the web-server is always running ✅

![Green](./images/green.png)

After using : `sudo docker container stop 2ecb11de1fae`

![Red](./images/red.png)

## architecture diagram (Generate with terraform graph and graphviz)
![Schema](./images/graph.svg.png)


## Name
My work for the "Data Ops - Technical Test" at doctolib.

## How to use : 
1. Clone the repo 
2. create your specific run_service_generique with your own @username:
```
cd doctolib-dataops-terraform-project
cp -r run_service_generique run_service_<@username>
```
3. Edit the generic terraform-project/run_service_<@username>/env.sh file with your informations
4. Go inside your directory and use terraform
```
cd terraform-project/run_service_<@username>
terraform init
terraform plan
terraform apply
```
5. You can find your ec2 fqdn using : 
```
cat terraform.tfstate | grep ec2-
```
6. Connect to you vm using SSH : 
```
ssh -i id_rsa ubuntu@<EC2 fqdn from the previous step>
```
7. Call the webservice : 
```
curl http://localhost:1080/mockserver/dashboard
```
![api-call](./images/api_call_example.png)

8. Verify health checks in the aws console : 
```
go to https://console.aws.amazon.com/route53/healthchecks/home#/ using your browser and see the healthchecks status
```

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

### Things not again working : 
* avoid to recreate aws instance when user_data install are modified 🔴

* Use a [terraform remote state](https://www.terraform.io/language/state/remote-state-data)

* Mount a [S3 as a FS](https://github.com/s3fs-fuse/s3fs-fuse)

* use as a terraform module 

## Author
UZAN Raphaël

***

## Support
For any comments or questions you can contact me on this email address: raphael.uzan.aa@gmail.com

***
